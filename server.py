import os
import json

try:
  from SimpleHTTPServer import SimpleHTTPRequestHandler as Handler
  from SocketServer import TCPServer as Server
except ImportError:
  from http.server import SimpleHTTPRequestHandler as Handler
  from http.server import HTTPServer as Server

# Read port selected by the cloud for our application
PORT = int(os.getenv('PORT', 8000))
# Change current directory to avoid exposure of control files
os.chdir('static')

class MyHandler(Handler):

    def do_GET(self):
        print "GET"
        print self.path
        body = b'Hello World'
        self.send_response(200)
        self.send_header('Content-type', 'text/html; charset=utf-8')
        self.send_header('Content-length', len(body))
        self.end_headers()
        self.wfile.write(body)

    def do_POST(self):
        print "POST"
        content_len = int(self.headers.getheader('content-length', 0))
        post_body = self.rfile.read(content_len)
        test_data = json.loads(post_body)

        if test_data["commingType"] == "pepper":
            print "pepper"
            print test_data

        elif test_data["commingType"] == "sensor":
            print test_data

        print "post_body(%s)" % (test_data)
        self.send_response(200)
        self.send_header('Content-type', 'text/html; charset=utf-8')
        self.send_header('Content-length', len(test_data))
        self.end_headers()
        self.wfile.write(test_data)

httpd = Server(("", PORT), MyHandler)
try:
  print("Start serving at port %i" % PORT)
  httpd.serve_forever()
except KeyboardInterrupt:
  pass
httpd.server_close()

